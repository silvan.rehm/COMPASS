# Mise En Place

- concat all data into one .npz with **15 min time interval**
- interpolate data with bigger stepsize

1. identify headers over time (z.B. 2015 Kohlegas, Erdgas .. 2018 nur noch Gas)
2. rename and merge (i.e. Kohlegas, Erdgas -> Gas)
3. store as master.npz

# Data Sources:

Residuallast -> Summe von konventionell

- installierte_Leistungen_2015-2022
  - pro Jahr
  - springt
  - -> Interpolate for 15 min values
  
Stromerzeugung_Verbrauch_15min_DE_2015-2022
  - Quelle für Verbrauch & Erzeugung


- Stromerzeugung_Jahreswerte_DE_2015-2022
  - pro jahr
  - spring
  - überflüssig
  
- Stromverbrauch_DE_2015-2022
  - Stündlich
  - überflüssig

Installierte_Leistungen_Kapazit%C3%A4tsfaktoren_2015-2022.xlsx)

# BackLog

Virtual installed source by interpolating year data over 15 mins -> plot for sanity check