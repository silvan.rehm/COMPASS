
input_file: str = "/home/silvan/ownCloud/YellowSubmarine/Daten/Stromerzeugung_Verbrauch_15min_DE_2015-2022/energy-charts_Öffentliche_Nettostromerzeugung_in_Deutschland_2015.xlsx"
output_folder: str =  "~/python/COMPASS/data/"

import openpyxl
import numpy as np
from datetime import datetime
import os
import matplotlib.pyplot as plt


output_folder = os.path.expanduser(output_folder)
os.makedirs(output_folder, exist_ok=True)

def import_xlsx(input_file, output_folder):
    
    # Load the workbook and select the active sheet
    workbook = openpyxl.load_workbook(input_file)

    # Load the workbook and select the active sheet

    sheet = workbook.active

    # Extract column headers
    headers = [cell.value for cell in sheet[1]]
    print(headers)

    # Define the structured array dtype
    dtype = [('Datum (MEZ)', 'datetime64[ms]')] + [(header, 'float') for header in headers[1:]]

    # Read the data
    data = []
    for row in sheet.iter_rows(min_row=3):  # Skip header row
        row_data = (row[0].value,) + tuple(float(cell.value or 0) for cell in row[1:])  # Use cell.value directly for the date
        data.append(row_data)

    # Convert to NumPy structured array
    np_array = np.array(data, dtype=dtype)
    # Save the numpy array in a .npz file
    npz_filename = os.path.join(output_folder, 'netto_production.npz')
    np.savez(npz_filename, np_array)
    return np_array

if not os.path.exists(os.path.join(output_folder, 'netto_production.npz')):
    print("no .npz found, importing")
    np_array = import_xlsx(input_file, output_folder)
else:
    print("npz found, loading")
    np_array = np.load(os.path.join(output_folder, 'netto_production.npz'))['arr_0']
# Now you can access columns by name
print(np_array['Kernenergie'])

# Plot each column against time
time_column = np_array['Datum (MEZ)']

# Prepare a single plot for all columns
plt.figure(figsize=(12, 8))
time_column = np_array['Datum (MEZ)']

for name in np_array.dtype.names:
    if name != 'Datum (MEZ)' and name != 'Last':
        plt.plot(time_column, np_array[name], label=name, alpha = 0.5)

plt.xlabel('Time')
plt.ylabel('Values')
plt.title('All Columns vs Time')
plt.legend()
plt.grid(True)

# Save the combined plot in the output_folder
plot_filename = os.path.join(output_folder, 'all_columns_vs_time.png')
plt.savefig(plot_filename)
plt.close()


